titantools (4.0.11+notdfsg1-7) unstable; urgency=medium

  * Move to Debhelper compatibility version 13 (Closes: #1002884)
  * debian/control: 
    - Updated Standards version (no changes required)
    - Add debhelper-compat = 13, remove debian/comat
  * debian/source/format - add and declare the format in use (1.0)

 -- Javier Fernández-Sanguino Peña <jfs@debian.org>  Sun, 27 Mar 2022 20:14:22 +0200

titantools (4.0.11+notdfsg1-6) unstable; urgency=medium

  * debian/control:
    - Remove the noshell transitional package as some releases have passed
      already
    - Use UTF-8 for maintainer's name
    - Add debhelper 9 dependency
  * Rewrite debian/rules to use the debhelper tools (with some overrides)
    (Closes: #822032)
  * Remove override for noshell since it is not build staticly

 -- Javier Fernández-Sanguino Peña <jfs@debian.org>  Sat, 23 Apr 2016 19:32:42 +0200

titantools (4.0.11+notdfsg1-5) unstable; urgency=low

  * debian/control: Add noshell to the correct section in non-free
    (Closes: 620178)

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Thu, 31 Mar 2011 00:38:03 +0200

titantools (4.0.11+notdfsg1-4) unstable; urgency=low

  * Rename the binary package to 'titantools' as noshell is just
    one of the tools provided within it.
  * debian/README.Debian: rewrite for clarity and fix some grammatical typos
  * debian/noshell.sgml: fix grammatical errors and clarify the content and 
    examples. Also properly document the (minor) differences between
    noshell and nologin. (Closes: #410221)
  * debian/postinst: Change the pointer to the new README file location
  * debian/rules: Now use debian/titantools as the directory for building
    the package

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Tue, 29 Mar 2011 22:46:21 +0200

titantools (4.0.11+notdfsg1-3) unstable; urgency=low

  * Update maintainer's address.
  * runas.8: Fix typo

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Tue, 29 Mar 2011 01:40:11 +0200

titantools (4.0.11+notdfsg1-2) unstable; urgency=low

  * Add 'XS-Autobuild: yes' to debian/control so this can get autobuilt
    even if in non-free.

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Thu, 02 Dec 2010 03:34:33 +0100

titantools (4.0.11+notdfsg1-1) unstable; urgency=low

  * Artificial upstream version to make it possible for titantools to
    move from main to non-free.
  * Makefile.linux: Do not build noshell as a static binary as this
    does not gain anything and leads to a FTBFS in amd64 (Closes: 584383)
  * Remove configure and configure-stamp targets from debian/rules
  * debian/control: 
      - Clarify that noshell is an alternative to 'nologin' which is now
        provided by the login package.
      - Remove the Homepage entry as http://www.fish2.com/titan/, since it is
        no longer available and no homepage can be found to replace it.
  * debian/noshell.sgml:
       - Document differences between noshell and nologin and add shells, login
         and nologin in SEE ALSO.
       - Change my email address
       - Comment out the old sites which are no longer available
  * debian/noshell.sgml:
       - Change my email address
       - Comment out the old sites which are no longer available
  * debian/rules: use dh_prep instead of dh_clean -k
  * Use Debhelper compatibility version 5, no changes needed.
  * Upgrade to Standards Version 3.9.1.0:
     - Convert debian/copyright to UTF-8

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Mon, 30 Aug 2010 16:35:53 +0200

titantools (4.0.11-6) unstable; urgency=low

  * Change to the license file, note that upstream has changed the license
    as of january 2005, even though we are not releasing the latest
    source code it makes no sense to distribute these tools with the 
    old license. (Closes: #397188)
  * Move the package to non-free due to the license change
  * As suggested by Justin Pryzby, change the recommendation
    at README.Debian to tell users not to include noshell in /etc/shells
    and add postinst code to detect users that followed the
    previous (wrong) advice. (Closes: #429697)
  * Change references to old site to the new one (http://www.fish2.com/titan/)
  * Move Homepage to the control: header and also update the location 
    of upstream's project
  * Ammend the manpage's description of the runas command (Closes: #374621)

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Tue, 03 Jun 2008 01:31:41 +0200

titantools (4.0.11-5) unstable; urgency=low

  * Remove useless runas.1 manpage
  * Fixed debian/rules, it was removing the wrong file
  * Fixed runas' manpage (Closes: #374621)

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Tue, 19 Jun 2007 17:41:27 +0200

titantools (4.0.11-4) unstable; urgency=low

  * Update location of the Titan homepage, it is now
    http://www.trouble.org/titan/ (Closes: #396362)

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Wed,  1 Nov 2006 10:37:56 +0100

titantools (4.0.11-3) unstable; urgency=low

  * Minor manpage fixes.

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Wed,  3 May 2006 13:23:24 +0200

titantools (4.0.11-2) unstable; urgency=low

  * Added proper Build-Depends (Closes: #225743)

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Fri,  2 Jan 2004 11:29:41 +0100

titantools (4.0.11-1) unstable; urgency=low

  * Initial Release.
  * Modified Noshell.c in order to generate the syslog warning as
  'noshell' and not Titan.
  * Modified the Makefile in order to add a 'clean' target, also
  runas is not compiled staticly (until I figure out the errors it
  generates)
  * Wrote manpages for runas and noshell

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Thu, 20 Nov 2003 08:52:17 +0100

