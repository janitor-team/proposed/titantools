# Titan Makefile for Solaris
VERSION = 4.0

CC = cc
CPPFLAGS =
CFLAGS   = 
LDFLAGS  = -dn
LIBS     = -Bstatic /usr/lib/libc.a -Bstatic /usr/ccs/lib/libtermcap.a -Bstatic /usr/lib/libnsl.a
LIBS2    = /usr/lib/libc.a 


# default target
all:  noshell runas

noshell: noshell.o
	$(CC) -Bstatic $(CFLAGS) -o noshell $(LIBS) $(LDFLAGS) noshell.o

runas: stubs.o runas.o
	$(CC) $(CFLAGS) -o runas $(LIBS2) $(LDFLAGS) stubs.o runas.o

